<?php

namespace Alexo\LaravelAgileCRM\Tests;

use Alexo\LaravelAgileCRM\LaravelAgileCRM;
use Alexo\LaravelAgileCRM\Exceptions\LaravelAgileCRMException;
use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

class LaravelAgileCRMTest extends TestCase
{
    private $customer = [
        'lead_score'=> '10',
        'star_value' => '5',
        'properties' => [
            [
                'name' => 'first_name',
                'value' => 'John Doe',
                'type' => 'SYSTEM'
            ],
            [
                'name' => 'email',
                'value' => 'johndoe@test.com',
                'type' => 'SYSTEM'
            ]
        ]
    ];

    private $customerUpdated = [
        'properties' => [
            [
                'name' => 'first_name',
                'value' => 'Bill Gates',
                'type' => 'SYSTEM'
            ],
            [
                'name' => 'email',
                'value' => 'bill@test.com',
                'type' => 'SYSTEM'
            ]
        ]
    ];

    private $company = [
        'type' => 'COMPANY',
        'properties' => [
            [
                'name' => 'name',
                'value' => 'test company',
                'type' => 'SYSTEM'
            ]
        ]
    ];

    public static function setUpBeforeClass()
    {
        if (file_exists(__DIR__.'/../.env')) {
            $dotenv = new Dotenv(__DIR__.'/../');
            $dotenv->load();
        }
    }

    public function testItFailsOnEmptyContact()
    {
        $this->expectException(LaravelAgileCRMException::class);

        $contact_json = [];
        LaravelAgileCRM::createContact($contact_json);
    }

    public function testItFailsOnIncompleteContact()
    {
        $this->expectException(LaravelAgileCRMException::class);

        $contact_json = [
            'properties' => []
        ];

        LaravelAgileCRM::createContact($contact_json);
    }

    public function testItFailsOnIncompleteNameContact()
    {
        $this->expectException(LaravelAgileCRMException::class);

        $contact_json = [
            'properties' => [
                [
                    'name' => 'first_name',
                    'value' => ''
                ]
            ]
        ];

        LaravelAgileCRM::createContact($contact_json);
    }

    public function testItFailsOnIncompleteEmailContact()
    {
        $this->expectException(LaravelAgileCRMException::class);

        $contact_json = [
            'properties' => [
                [
                    'name' => 'email',
                    'value' => ''
                ]
            ]
        ];

        LaravelAgileCRM::createContact($contact_json);
    }

    public function testCanListCompanies()
    {
        $form_fields = [
            'page_size' => urlencode("25"),
            'global_sort_key' => urlencode("-created_time"),
        ];

        $fields_string1 = '';
        foreach ($form_fields as $key => $value) {
            $fields_string1 .= $key . '=' . $value . '&';
        }

        $companies = LaravelAgileCRM::listCompanies(rtrim($fields_string1, '&'));
        $this->assertNotNull($companies);
        return $companies;
    }

    /**
     * @depends testCanListCompanies
    */
    public function testCanListCompanyContacts(string $companies)
    {
        $companies = json_decode($companies);
        $contacts = LaravelAgileCRM::findContactsInCompany($companies[1]->id);
        echo "$contacts";
        $this->assertNotNull($companies);
    }

    public function testCanCreateContact()
    {
        $contact = LaravelAgileCRM::createContact($this->customer);
        $this->assertNotNull($contact);
        return $contact;
    }

    /**
     * @depends testCanCreateContact
    */
    public function testCanFindContact(string $contact)
    {
        $res = LaravelAgileCRM::find(json_decode($contact)->id);
        $this->assertNotNull($res);
    }

    /**
     * @depends testCanCreateContact
    */
    public function testCanUpdateStar(string $contact)
    {
        $contactStar = [
            'id' => json_decode($contact)->id,
            'star_value' => 4
        ];

        $res = LaravelAgileCRM::updateStarValue($contactStar);
        $this->assertNotNull($res);
    }

    /**
     * @depends testCanCreateContact
    */
    public function testCanUpdateScore(string $contact)
    {
        $contactScore = [
            'id' => json_decode($contact)->id,
            'lead_score' => 20
        ];

        $res = LaravelAgileCRM::updateLeadScore($contactScore);
        $this->assertNotNull($res);
    }

    /**
     * @depends testCanCreateContact
    */
    public function testCanUpdateTags(string $contact)
    {
        $contactTags = [
            'id' => json_decode($contact)->id,
            'tags' => ['hello', 'hi']
        ];

        $res = LaravelAgileCRM::updateTags($contactTags);
        $this->assertNotNull($res);
    }

    /**
     * @depends testCanCreateContact
    */
    public function testCanUpdateTagsByEmail(string $contact)
    {
        $contactTags = [
            'email' => urlencode($this->customer['properties']['1']['value']),
            'tags' => urlencode("['world', 'bye']")
        ];

        $fields_string = '';
        foreach ($contactTags as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        $res = LaravelAgileCRM::updateContactTagsByEmail(rtrim($fields_string, '&'));
        $this->assertNotNull($res);
        return $res;
    }

    /**
     * @depends testCanUpdateTagsByEmail
    */
    public function testCanDeleteTagsByEmail(string $tags)
    {
        $contactTags = [
            'email' => urlencode($this->customer['properties']['1']['value']),
            'tags' => urlencode("['hi', 'bye']")
        ];

        $fields_string = '';
        foreach ($contactTags as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        $res = LaravelAgileCRM::deleteContactTagsByEmail(rtrim($fields_string, '&'));
        $this->assertNotNull($res);
    }

    /**
     * @depends testCanCreateContact
    */
    public function testCanUpdateContact(string $contact)
    {
        $this->customerUpdated['id'] = json_decode($contact)->id;
        $res = LaravelAgileCRM::update($this->customerUpdated);

        $this->assertNotNull($res);
        return $res;
    }

    /**
     * @depends testCanUpdateContact
    */
    public function testCanDeleteContact(string $contact)
    {
        $res = LaravelAgileCRM::delete(json_decode($contact)->id);
        $this->assertNotNull($res);
    }
}
