<?php

namespace Alexo\LaravelAgileCRM;

use Alexo\LaravelAgileCRM\Agile\Contact;
use Alexo\LaravelAgileCRM\Agile\Company;
use Alexo\LaravelAgileCRM\Agile\Shared;
use GuzzleHttp\Client;

class LaravelAgileCRM
{
	/**
     * The agilecrm API URL.
     *
     * @var string
     */
    protected static $apiURL;

    /**
     * The agilecrm API User(email).
     *
     * @var string
     */
    protected static $apiUser;

	/**
     * The agilecrm API Key.
     *
     * @var string
     */
    protected static $apiKey;

    /**
     * The Guzzle Client.
     *
     * @var GuzzleHttp\Client
     */
    protected static $httpClient;

    /**
     * Get the API URL.
     *
     * @return string
     */
    public static function getApiUrl()
    {
        if (static::$apiURL) {
            return static::$apiURL;
        }

        if ($apiURL = getenv('AGILE_URL')) {
            return $apiURL;
        }

        return config('agile.api_url');
    }

    /**
     * Get the API User.
     *
     * @return string
     */
    protected static function getApiUser()
    {
        if (static::$apiUser) {
            return static::$apiUser;
        }

        if ($apiUser = getenv('AGILE_USER')) {
            return $apiUser;
        }

        return config('agile.api_user');
    }

    /**
     * Get the API key.
     *
     * @return string
     */
    protected static function getApiKey()
    {
        if (static::$apiKey) {
            return static::$apiKey;
        }

        if ($apiKey = getenv('AGILE_KEY')) {
            return $apiKey;
        }

        return config('agile.api_key');
    }

    public static function getHttpClient()
    {
    	if (!isset(self::$httpClient)) {
            if (is_null(self::getApiUser())) {
                throw new LaravelAgileCRMException('Missing Agile API user credentials.');
            }

            if (is_null(self::getApiKey())) {
                throw new LaravelAgileCRMException('Missing Agile API key credentials.');
            }

            if (is_null(self::getApiUrl())) {
                throw new LaravelAgileCRMException('Missing Agile API domain.');
            }

            $user = self::getApiUser();
            $key = self::getApiKey();

            self::$httpClient = new Client(['auth' => [$user, $key]]);
        }

        return self::$httpClient;
    }

    //CONTACTS & COMPANIES
    public static function find($id)
    {
        return Shared::fetchById($id);
    }

    public static function delete($id)
    {
        return Shared::delete($id);
    }

    public static function search($query, $type)
    {
        return Shared::search($query, $type);
    }

    public static function update($data)
    {
        return Shared::update($data);
    }

    public static function updateStarValue($data)
    {
        return Shared::updateStar($data);
    }

    public static function updateLeadScore($data)
    {
        return Shared::updateScore($data);
    }

    public static function updateTags($data)
    {
        return Shared::updateTags($data);
    }

    //CONTACTS
    public static function createContact($data)
    {
        return Contact::create($data);
    }

    public static function findContactByEmail($email)
    {
        return Contact::fetchByEmail($email);
    }

    public static function updateContactTagsByEmail($data)
    {
        return Contact::updateTagsByEmail($data);
    }

    public static function deleteContactTagsByEmail($data)
    {
        return Contact::deleteTagsByEmail($data);
    }

    //COMPANIES
    public static function createCompany($data)
    {
        return Company::create($data);
    }

    public static function listCompanies($data)
    {
        return Company::list($data);
    }

    public static function findContactsInCompany($id)
    {
        return Company::getContacts($id);
    }
}
