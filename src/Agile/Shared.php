<?php

namespace Alexo\LaravelAgileCRM\Agile;

use Alexo\LaravelAgileCRM\Exceptions\LaravelAgileCRMException;
use Alexo\LaravelAgileCRM\LaravelAgileCRM;
use Exception;

class Shared
{
    public static function fetchById($id)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/'.$id;
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);

            return $res->getBody();
        } catch (Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function update($data)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/edit-properties';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('PUT', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'json' => $data
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function updateStar($data)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/edit/add-star';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('PUT', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'json' => $data
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function updateScore($data)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/edit/lead-score';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('PUT', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'json' => $data
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function updateTags($data)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/edit/tags';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('PUT', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'json' => $data
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function delete($id)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/'.$id;
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('DELETE', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function search($query, $type)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/search?q='.$query.'&page_size=10&type="'.$type.'"';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }
}
