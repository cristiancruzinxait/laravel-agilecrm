<?php

namespace Alexo\LaravelAgileCRM\Agile;

use Alexo\LaravelAgileCRM\Exceptions\LaravelAgileCRMException;
use Alexo\LaravelAgileCRM\LaravelAgileCRM;
use Exception;

class Contact
{
    public static function create($data)
    {
        if (!self::hasMinimumValidData($data)) {
            throw new LaravelAgileCRMException('Missing minimum data to create contact.');
        }

        $email = null;

        foreach ($data['properties'] as $item) {
            if (array_key_exists('name', $item) && $item['name'] == 'email') {
                $email = $item['value'];
            }
        }

        $contactSearch = self::fetchByEmail($email);

        if ($contactSearch == '') {
            try {
                $url =  LaravelAgileCRM::getApiUrl().'/contacts';
                $client = LaravelAgileCRM::getHttpClient();
                $res = $client->request('POST', $url, [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                    'json' => $data
                ]);

                return $res->getBody();
            } catch(Exception $exc) {
                throw new LaravelAgileCRMException($exc->getMessage());
            }
        }
    }

    public static function fetchByEmail($email)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/search/email/'.$email;
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function updateTagsByEmail($data)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/email/tags/add';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('POST', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'body' => $data
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function deleteTagsByEmail($data)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/email/tags/delete';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('POST', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'body' => $data
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    protected static function hasMinimumValidData($data)
    {
        $isValid = true;

        if (!array_key_exists('properties', $data)) {
            $isValid = false;
        } else {
            if (!is_array($data['properties'])) {
                $isValid = false;
            } else {
                $hasName = false;
                $hasEmail = false;

                foreach ($data['properties'] as $item) {
                    if (is_array($item)) {
                        if (array_key_exists('name', $item) && $item['name'] == 'first_name') {
                            if (!is_null($item['value'])) {
                                $hasName = true;
                            }
                        }

                        if (array_key_exists('name', $item) && $item['name'] == 'email') {
                            if (!is_null($item['value'])) {
                                $hasEmail = true;
                            }
                        }
                    }
                }

                if (!$hasName || !$hasEmail) {
                    $isValid = false;
                }
            }
        }

        return $isValid;
    }
}
