<?php

namespace Alexo\LaravelAgileCRM\Agile;

use Alexo\LaravelAgileCRM\Exceptions\LaravelAgileCRMException;
use Alexo\LaravelAgileCRM\LaravelAgileCRM;
use Exception;

class Company
{
    public static function create($data)
    {
        if (!self::hasMinimumValidData($data)) {
            throw new LaravelAgileCRMException('Missing minimum data to create company.');
        }

        $name = null;

        foreach ($data['properties'] as $item) {
            if (array_key_exists('name', $item) && $item['name'] == 'name') {
                $name = $item['value'];
            }
        }

        $companySearch = LaravelAgileCRM::search($name, 'COMPANY');

        if (!count(json_decode($companySearch))) {
            try {
                $url =  LaravelAgileCRM::getApiUrl().'/contacts';
                $client = LaravelAgileCRM::getHttpClient();
                $res = $client->request('POST', $url, [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                    'json' => $data
                ]);

                return $res->getBody();
            } catch (Exception $exc) {
                throw new LaravelAgileCRMException($exc->getMessage());
            }
        }
    }

    public static function list($data)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/companies/list';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('POST', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'body' => $data
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    public static function getContacts($id)
    {
        try {
            $url =  LaravelAgileCRM::getApiUrl().'/contacts/related/'.$id.'?page_size=25';
            $client = LaravelAgileCRM::getHttpClient();
            $res = $client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);

            return $res->getBody();
        } catch(Exception $exc) {
            throw new LaravelAgileCRMException($exc->getMessage());
        }
    }

    protected static function hasMinimumValidData($data)
    {
        $isValid = true;

        if (!array_key_exists('properties', $data)) {
            $isValid = false;
        } else {
            if (!is_array($data['properties'])) {
                $isValid = false;
            } else {
                $hasName = false;

                foreach ($data['properties'] as $item) {
                    if (is_array($item)) {
                        if (array_key_exists('name', $item) && $item['name'] == 'name') {
                            if (!is_null($item['value'])) {
                                $hasName = true;
                            }
                        }
                    }
                }

                if (!$hasName) {
                    $isValid = false;
                }
            }
        }

        return $isValid;
    }
}
