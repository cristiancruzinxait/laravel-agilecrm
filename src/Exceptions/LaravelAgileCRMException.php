<?php

namespace Alexo\LaravelAgileCRM\Exceptions;

use Exception;

class LaravelAgileCRMException extends Exception
{
}
